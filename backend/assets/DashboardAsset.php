<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
    ];
    public $js = [
        'js/main.js',
       'js/jquery.min.js',
        //'js/jquery-ui.min.js',
        'js/bootstrap.min.js',
        'js/bootstrap.js',
        'js/dashboard.js'
        //'plugins',
    ];
    public $depends = [
        'yii\web\YiiAsset',
       //'yii\bootstrap\BootstrapAsset',
    ];
}
