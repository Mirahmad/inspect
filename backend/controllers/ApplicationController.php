<?php

namespace backend\controllers;

use backend\models\District;
use backend\models\Educationfield;
use backend\models\Regions;
use Yii;
use backend\models\Application;
use backend\models\ApplicationSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\caching\FileCache;

use yii\data\ActiveDataProvider;

/**
 * ApplicationController implements the CRUD actions for Application model.
 */
class ApplicationController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($status)
    {
        $searchModel = new ApplicationSearch();

        if ($status == 'All')
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        else{
            $data = Application::find()
                ->where(['statusId'=> $status]);
            $dataProvider = new ActiveDataProvider([
                'query' => $data,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Application();

        if ($model->load(Yii::$app->request->post()) ) {

            $model->uniqueId=random_int(1000,999999999);

            $filename1 ="Appcilaction".$model->name;
            $model->fileApplicaton=UploadedFile::getInstance($model,'fileApplicaton');
            $model->fileApplicaton->saveAs('upload/'.$filename1.'.'.$model->fileApplicaton->extension);
            $model->applications= 'upload/'.$filename1.'.'.$model->fileApplicaton->extension;

            $filename2 ="Passport".$model->name;
            $model->filePass=UploadedFile::getInstance($model,'filePass');
            $model->filePass->saveAs('upload/'.$filename2.'.'.$model->filePass->extension);
            $model->passport= 'upload/'.$filename2.'.'.$model->filePass->extension;

            $filename3 ="Diploma".$model->uniqueId;
            $model->fileDiploma=UploadedFile::getInstance($model,'fileDiploma');
            $model->fileDiploma->saveAs('upload/'.$filename3.'.'.$model->fileDiploma->extension);
            $model->diploma= 'upload/'.$filename3.'.'.$model->fileDiploma->extension;

            $filename4 ="Appendix".$model->uniqueId;
            $model->fileAppendix=UploadedFile::getInstance($model,'fileAppendix');
            $model->fileAppendix->saveAs('upload/'.$filename4.'.'.$model->fileAppendix->extension);
            $model->appendix= 'upload/'.$filename4.'.'.$model->fileAppendix->extension;

            $model->save();
            return $this->redirect(['view', 'id' => $model->Id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLists($id){

        $countDis = District::find()
            ->where(['RegId'=>$id])
            ->count();
        $allReg =District::find()
            ->where(['RegId'=>$id])
            ->all();
        if ($countDis>0){
            foreach ($allReg as $itm)
                echo "<option value ='".$itm->Id. "'>".$itm->Name."</option>";
        }else{
            echo "<option>No selection</option>";
        }
    }

    public function actionListedu($id)
    {
        $countEdu = Educationfield::find()
            ->where(['KanowId'=>$id])
            ->count();
        $allEdu =Educationfield::find()
            ->where(['KanowId'=>$id])
            ->all();
        if ($countEdu>0){
            foreach ($allEdu as $itm)
                echo "<option value ='".$itm->Id. "'>".$itm->Name."</option>";

        }else{
            echo "<option>No selection</option>";


        }
    }

    public function actionDownloads($id,$md)
    {
        $download = Application::findOne($id);



        $path = Yii::getAlias('@frontend').'/web/'.$download->$md;

        print_r($path);

        if (file_exists($path))
        {
            return Yii::$app->response->sendFile($path);
            $this->render('_form', ['model' => $model, ]);
        }




    }


}
