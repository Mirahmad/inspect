<?php

namespace backend\models;

use Yii;


class Application extends \yii\db\ActiveRecord
{
    public $fileApplicaton;
    public $filePass;
    public $fileDiploma;
    public $fileAppendix;

    public static function tableName()
    {
        return 'application';
    }

    public function rules()
    {
        return [
            [['uniqueId', 'name', 'secondName', 'middleName', 'birthday', 'regionId', 'districtId', 'address', 'phoneNumber', 'email', 'universityname', 'countryId',
                'specialization', 'docnumber', 'docregnum', 'givendate', 'enddate', 'eduformId', 'degreeId', 'knowfieidId', 'doctypeId',
                'applications', 'passport', 'diploma', 'appendix', 'statusId', 'comId', 'acceptdate', 'changeddate'], 'required'],
            [['uniqueId', 'regionId', 'districtId', 'countryId', 'eduformId', 'degreeId', 'knowfieidId', 'educationfieldId', 'doctypeId', 'statusId', 'comId'], 'integer'],
            [['birthday', 'givendate', 'entrydate', 'enddate', 'acceptdate', 'changeddate'], 'safe'],
            [['address', 'specialization'], 'string'],
            [['name', 'passport', 'diploma', 'appendix'], 'string', 'max' => 255],
            [['secondName', 'middleName', 'universityname', 'docregnum'], 'string', 'max' => 100],
            [['phoneNumber', 'docnumber'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 150],
            [['docseriya'], 'string', 'max' => 10],
            [['applications'], 'string', 'max' => 300],
            [['uniqueId'], 'unique'],
            [['email'],'email'],

            [['fileApplicaton'],'file'],
            [['filePass'],'file'],
            [['fileDiploma'],'file'],
            [['fileAppendix'],'file'],

            [['eduformId'], 'exist', 'skipOnError' => true, 'targetClass' => Eduform::className(), 'targetAttribute' => ['eduformId' => 'Id']],
            [['degreeId'], 'exist', 'skipOnError' => true, 'targetClass' => Degree::className(), 'targetAttribute' => ['degreeId' => 'Id']],
            [['knowfieidId'], 'exist', 'skipOnError' => true, 'targetClass' => Knowfieid::className(), 'targetAttribute' => ['knowfieidId' => 'Id']],
            [['educationfieldId'], 'exist', 'skipOnError' => true, 'targetClass' => Educationfield::className(), 'targetAttribute' => ['educationfieldId' => 'Id']],
            [['regionId'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::className(), 'targetAttribute' => ['regionId' => 'Id']],
            [['doctypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Documenttyps::className(), 'targetAttribute' => ['doctypeId' => 'Id']],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'Id']],
            [['districtId'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['districtId' => 'Id']],
            [['comId'], 'exist', 'skipOnError' => true, 'targetClass' => Commission::className(), 'targetAttribute' => ['comId' => 'Id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'uniqueId' => 'Unique ID',
            'name' => 'Имя',
            'secondName' => 'Фамилия',
            'middleName' => 'Отчество',
            'birthday' => 'День Рождения',
            'regionId' => 'Область',
            'districtId' => 'Район',
            'address' => 'Адрес',
            'phoneNumber' => 'Номер телефона',
            'email' => 'Эл. адрес',
            'universityname' => 'Название Университета',
            'countryId' => 'Страна',
            'specialization' => 'Специализация',
            'docseriya' => 'Док Серия ',
            'docnumber' => 'Номер Док',
            'docregnum' => 'Регистрационный Номер Документа',
            'givendate' => 'Док Данная Дата',
            'entrydate' => 'Дата ввода',
            'enddate' => 'Дата окончания',
            'eduformId' => 'Форма обучения',
            'degreeId' => 'Ученая степень',
            'knowfieidId' => 'Область знаний',
            'educationfieldId' => 'Образование',
            'doctypeId' => 'Тип документа',
            'applications' => 'приложений',
            'passport' => 'Паспорт',
            'diploma' => 'Диплом',
            'appendix' => 'Аппендикс',
            'statusId' => 'Положение дел',
            'comId' => 'Комиссия',
            'acceptdate' => 'Дата принятия',
            'changeddate' => 'Дата изменения',
        ];
    }


    public function getEduform()
    {
        return $this->hasOne(Eduform::className(), ['Id' => 'eduformId']);
    }


    public function getDegree()
    {
        return $this->hasOne(Degree::className(), ['Id' => 'degreeId']);
    }


    public function getKnowfieid()
    {
        return $this->hasOne(Knowfieid::className(), ['Id' => 'knowfieidId']);
    }


    public function getEducationfield()
    {
        return $this->hasOne(Educationfield::className(), ['Id' => 'educationfieldId']);
    }


    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['Id' => 'regionId']);
    }

    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['Id' => 'countryId']);
    }


    public function getDoctype()
    {
        return $this->hasOne(Documenttyps::className(), ['Id' => 'doctypeId']);
    }


    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['Id' => 'statusId']);
    }


    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['Id' => 'districtId']);
    }


    public function getCom()
    {
        return $this->hasOne(Commission::className(), ['Id' => 'comId']);
    }
}
