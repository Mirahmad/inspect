<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Application;

/**
 * ApplicationSearch represents the model behind the search form about `backend\models\Application`.
 */
class ApplicationSearch extends Application
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'uniqueId', 'regionId', 'districtId', 'countryId', 'eduformId', 'degreeId', 'knowfieidId', 'educationfieldId', 'doctypeId', 'statusId', 'comId'], 'integer'],
            [['name', 'secondName', 'middleName', 'birthday', 'address', 'phoneNumber', 'email', 'universityname', 'specialization', 'docseriya', 'docnumber', 'docregnum', 'givendate', 'entrydate', 'enddate', 'applications', 'passport', 'diploma', 'appendix', 'acceptdate', 'changeddate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Application::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Id' => $this->Id,
            'uniqueId' => $this->uniqueId,
            'birthday' => $this->birthday,
            'regionId' => $this->regionId,
            'districtId' => $this->districtId,
            'countryId' => $this->countryId,
            'givendate' => $this->givendate,
            'entrydate' => $this->entrydate,
            'enddate' => $this->enddate,
            'eduformId' => $this->eduformId,
            'degreeId' => $this->degreeId,
            'knowfieidId' => $this->knowfieidId,
            'educationfieldId' => $this->educationfieldId,
            'doctypeId' => $this->doctypeId,
            'statusId' => $this->statusId,
            'comId' => $this->comId,
            'acceptdate' => $this->acceptdate,
            'changeddate' => $this->changeddate,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'secondName', $this->secondName])
            ->andFilterWhere(['like', 'middleName', $this->middleName])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phoneNumber', $this->phoneNumber])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'universityname', $this->universityname])
            ->andFilterWhere(['like', 'specialization', $this->specialization])
            ->andFilterWhere(['like', 'docseriya', $this->docseriya])
            ->andFilterWhere(['like', 'docnumber', $this->docnumber])
            ->andFilterWhere(['like', 'docregnum', $this->docregnum])
            ->andFilterWhere(['like', 'applications', $this->applications])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'diploma', $this->diploma])
            ->andFilterWhere(['like', 'appendix', $this->appendix]);

        return $dataProvider;
    }
}
