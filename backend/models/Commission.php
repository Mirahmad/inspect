<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "commission".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $Description
 *
 * @property Knowfieid[] $knowfies
 */
class Commission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description'], 'required'],
            [['Description'], 'string'],
            [['Name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Название Комиссии',
            'Description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKnowfies()
    {
        return $this->hasMany(Knowfieid::className(), ['comId' => 'Id']);
    }
}
