<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "educationfield".
 *
 * @property integer $Id
 * @property string $Name
 * @property integer $KanowId
 * @property string $nameRu
 *
 * @property Application[] $applications
 * @property Knowfieid $kanow
 */
class Educationfield extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'educationfield';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'KanowId', 'nameRu'], 'required'],
            [['KanowId'], 'integer'],
            [['Name'], 'string', 'max' => 250],
            [['nameRu'], 'string', 'max' => 100],
            [['KanowId'], 'exist', 'skipOnError' => true, 'targetClass' => Knowfieid::className(), 'targetAttribute' => ['KanowId' => 'Id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Образование',
            'KanowId' => 'Область знаний',
            'nameRu' => 'Образование(RU)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['educationfieldId' => 'Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKanow()
    {
        return $this->hasOne(Knowfieid::className(), ['Id' => 'KanowId']);
    }
}
