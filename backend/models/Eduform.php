<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "eduform".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $Description
 * @property string $nameRu
 *
 * @property Application[] $applications
 */
class Eduform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eduform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'nameRu'], 'required'],
            [['Description'], 'string'],
            [['Name', 'nameRu'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Форма обучения',
            'Description' => 'Описание',
            'nameRu' => 'Форма обучения (RU)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['eduformId' => 'Id']);
    }
}
