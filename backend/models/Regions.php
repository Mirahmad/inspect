<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $nameRu
 *
 * @property Application[] $applications
 * @property District[] $districts
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'nameRu'], 'required'],
            [['Name', 'nameRu'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Область',
            'nameRu' => 'Район(RU)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['regionId' => 'Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['RegId' => 'Id']);
    }
}
