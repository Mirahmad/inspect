<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;



$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Create Application',
            ['value'=>Url::to('index.php?r=application/create'),
                'class' => 'btn btn-success','id'=>'modalButton'])
        ?>
    </p>

    <?php
    Modal::begin(
        [
            'header' =>'<h4>Application</h4>',
            'id'=>'modal',
            'size'=>'modal-lg',
        ]);

    echo "<div id='modalContent'> </div>";
    Modal::end();
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'rowOptions'=>function($model){
            if($model->statusId==2){
                return ['class'=>'danger'];
            }
            if ($model->statusId==1)
            {
                return ['class'=>'success'];
            }
            if ($model->statusId==3){
                return ['class'=>'primary'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'Id',
            [
                'attribute' => 'name',
                'label' => 'ФИО',
                'value' => function($model) { return $model->name . ",". $model->secondName.",".$model->middleName ;},
            ],
            'birthday',
            [
                'attribute' => 'regionId',
                'value' =>'region.Name',
            ],
            [
                'attribute' => 'districtId',
                'value' =>'district.Name',
            ],



            //'address:ntext',
            'phoneNumber',
            'email:email',
            'universityname',
            // 'countryId',
            'specialization:ntext',
            //'docseriya',
            //'docnumber',
            //'docregnum',
            //'givendate',
            //'entrydate',
            //  'enddate',
            //'eduform.Name',
            //'degree.Name',
            //'knowfieid.Name',
            //'educationfield.Name',
            //'comId',
            //'doctype.Name',
            //'Documents',
            //'uniqueId',
            //'status.Name',
            //'acceptdate',
            //'changeddate',
            [
                'label' => 'Положение дел',
                'format' => 'raw',
                'value' => function($model) {
                    if ($model->statusId == 1){
                        return Html::a('Confirmed', ['application/update', 'id' => $model->Id], ['class' => 'btn btn-success']);}
                    if ($model->statusId == 2){
                        return Html::a('Unconformed', ['application/update', 'id' => $model->Id], ['class' => 'btn btn-danger']);}
                    if ($model->statusId == 3){
                        return Html::a('Waiting', ['application/update', 'id' => $model->Id], ['class' => 'btn btn-primary']);}
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>





</div>
