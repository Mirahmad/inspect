<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Application */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'Id',
            'uniqueId',
            'name',
            'secondName',
            'middleName',
            'birthday',
            'region.Name',
            'district.Name',
            'address:ntext',
            'phoneNumber',
            'email:email',

            'universityname',
            'country.CountryName',
            'specialization:ntext',
            'docseriya',
            'docnumber',
            'docregnum',
            'givendate',
            'entrydate',
            'enddate',
            'eduform.Name',
            'degree.Name',
            'knowfieid.Name',
            'educationfield.Name',
            'doctype.Name',

            [
                'label' => 'Applications',
                'attribute' => 'applications',
                'format' => 'html',
                'value'=>  function($model){
                    return Html::a('скачать', ['application/downloads', 'id' => $model->Id,'md'=>'applications']);
                }

            ],

            [
                'label' => 'Passport',
                'attribute' => 'passport',
                'format' => 'html',
                'value'=>  function($model){
                    return Html::a('скачать', ['application/downloads', 'id' => $model->Id,'md'=>'passport']);
                }

            ],

            [
                'label' => 'Diploma',
                'attribute' => 'diploma',
                'format' => 'raw',
                'value'=>  function($model){
                    return Html::a('скачать', ['application/downloads', 'id' => $model->Id,'md'=>'diploma']);
                }

            ],

            [
                'label' => 'Appendix',
                'attribute' => 'appendix',
                'format' => 'html',
                'value'=>  function($model){
                    return Html::a('скачать', ['application/downloads', 'id' => $model->Id,'md'=>'appendix']);
                }

            ],

            'status.Name',
            'com.Name',
            'acceptdate',
            'changeddate',
        ],
    ]) ?>

</div>
