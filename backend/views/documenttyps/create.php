<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Documenttyps */

$this->title = 'Create Documenttyps';
$this->params['breadcrumbs'][] = ['label' => 'Documenttyps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documenttyps-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
