<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DocumenttypsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documenttyps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documenttyps-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Documenttyps', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'Name',
            'Description',
            'nameRu',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
