<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Knowfieid;

/* @var $this yii\web\View */
/* @var $model backend\models\Educationfield */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educationfield-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KanowId')->dropDownList(
            ArrayHelper::map(Knowfieid::find()->all(),'Id','Name'),
             ['prompt'=>'Select Talim soxasi']

    ) ?>

    <?= $form->field($model, 'nameRu')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
