<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Educationfield */

$this->title = 'Create Educationfield';
$this->params['breadcrumbs'][] = ['label' => 'Educationfields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educationfield-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
