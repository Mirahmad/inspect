<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EducationfieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Educationfields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educationfield-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Educationfield', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'Name',
            'kanow.Name',
            'nameRu',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
