<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Eduform */

$this->title = 'Create Eduform';
$this->params['breadcrumbs'][] = ['label' => 'Eduforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eduform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
