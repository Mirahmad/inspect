<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Commission;

/* @var $this yii\web\View */
/* @var $model backend\models\Knowfieid */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="knowfieid-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nameRu')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model,'comId')->dropDownList(
        ArrayHelper::map(Commission::find()->All(),'Id','Name'),
             ['prompt'=>'Select Commmission']
    ) ?>


    <?= $form->field($model, 'Description')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
