<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Knowfieid */

$this->title = 'Create Knowfieid';
$this->params['breadcrumbs'][] = ['label' => 'Knowfieids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="knowfieid-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
