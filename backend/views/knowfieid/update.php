<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Knowfieid */

$this->title = 'Update Knowfieid: ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Knowfieids', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->Id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="knowfieid-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
