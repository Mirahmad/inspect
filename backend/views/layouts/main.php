<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\assets\DashboardAsset;

use kartik\sidenav\SideNav;
use yii\helpers\Url;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../views/styles/addstyle.css">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body  class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>


<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top nav-blue',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="left-content">
            <?php
            echo SideNav::widget([
                'type' => SideNav::TYPE_SUCCESS,
                'heading' => 'Menu',
                'items' => [
                    [
                        //'url' => Url::to(['application/index']),
                        'label' => 'Applications',
                        'icon' => 'home',
                        'items' => [
                            ['label' => 'Wait', 'icon'=>'info-sign', 'url'=>Url::to(['application/index', 'status' => '3'])],
                            ['label' => 'Confirmed', 'icon'=>'phone', 'url'=>Url::to(['application/index', 'status' => '1'])],
                            ['label' => 'Unconfirmed', 'icon'=>'phone', 'url'=>Url::to(['application/index', 'status' => '2'])],
                            ['label' => 'All', 'icon'=>'phone', 'url'=>Url::to(['application/index', 'status' => 'All'])],
                        ],
                    ],
                    [
                        'url' => Url::to(['regions/index']),
                        'label' => 'Regions',
                        'icon' => 'home'
                    ],
                    //-----------------------------------------
                    [
                        'url' => Url::to(['status/index']),
                        'label' => 'Status',
                        'icon' => 'home'

                            ],
                    [
                        'url' => Url::to(['knowfieid/index']),
                        'label' => 'Knowfieid',
                        'icon' => 'home'

                    ],
                    [
                        'url' => Url::to(['educationfield/index']),
                        'label' => 'Educationfields',
                        'icon' => 'home'
                    ],
                    [
                            'url' => Url::to(['eduform/index']),
                        'label' => 'Eduforms',
                        'icon' => 'home'
                    ],

                    [
                        'url' => Url::to(['documenttyps/index']),
                        'label' => 'Documenttyps',
                        'icon' => 'home'
                    ],
                    [
                        'url' => Url::to(['district/index']),
                        'label' => 'Districts',
                        'icon' => 'home'
                    ],
                    [
                        'url' => Url::to(['degree/index']),
                        'label' => 'Degrees',
                        'icon' => 'home'

                    ],
                    [
                        'url' => Url::to(['countries/index']),
                        'label' => 'Countries',
                        'icon' => 'home'
                    ],
                    [
                        'url' => Url::to(['commission/index']),
                        'label' => 'Commission',
                        'icon' => 'home'

                    ],
                    [
                        'label' => 'Reportting',
                        'icon' => 'question-sign',
                        'items' => [
                            ['label' => 'About', 'icon'=>'info-sign', 'url'=>'#'],
                            ['label' => 'Contact', 'icon'=>'phone', 'url'=>'#'],
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
        <div class="main-content">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>





<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
