<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
       /* 'assets/3c58d997/css/bootstrap.css',
        'css/animate.css',
        'css/font-awesome.css',
        'css/jquery-ui.css',
        'css/animate.css',
        'css/font-awesome.css',
        'css/media.css'*/




    ];
    public $js = [
        'js/main.js',
        /*'js/jquery-2.1.3.min.js',
        'assets/c8a834d/jquery.js',
        'assets/1c73605d/yii.js',
        'js/jquery-ui.js',
        'js/jquery.cookie.js',
        'js/jQuery.print.js',
        'js/mousetrap.min.js',
        'js/specialView.js',
        'js/jquery.vmap.js',
        'js/waypoints.min.js',
        'js/map.js',
        'js/orphus.js',
        'js/pie.js',
        'js/scroll.js',
        'js/Chart.js',
        'js/highcharts.js',
        'js/exporting.js',
        'js/highcharts-more.js',
        'js/zoom.js',
        'js/main.js',
        'js/hoverIntent.js',
        'assets/3c58d997/js/bootstrap.js',*/
        ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
