<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $Id
 * @property string $CountryName
 * @property string $countrynameRu
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CountryName', 'countrynameRu'], 'required'],
            [['CountryName', 'countrynameRu'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'CountryName' => 'Country Name',
            'countrynameRu' => 'Countryname Ru',
        ];
    }
}
