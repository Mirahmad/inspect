<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "district".
 *
 * @property integer $Id
 * @property string $Name
 * @property integer $RegId
 * @property string $nameRu
 *
 * @property Application[] $applications
 * @property Regions $reg
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'RegId', 'nameRu'], 'required'],
            [['RegId'], 'integer'],
            [['Name', 'nameRu'], 'string', 'max' => 100],
            [['RegId'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::className(), 'targetAttribute' => ['RegId' => 'Id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          //  'Id' => 'ID',
            'Name' => 'Tuman Nomi',
            'RegId' => 'Viloyatni Tanlang',
            'nameRu' => 'Tuman Nomini Rustili',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['districtId' => 'Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReg()
    {
        return $this->hasOne(Regions::className(), ['Id' => 'RegId']);
    }
}
