<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "documenttyps".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $Description
 * @property string $nameRu
 *
 * @property Application[] $applications
 */
class Documenttyps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documenttyps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'nameRu'], 'required'],
            [['Name', 'Description', 'nameRu'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'nameRu' => 'Name Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['doctypeId' => 'Id']);
    }
}
