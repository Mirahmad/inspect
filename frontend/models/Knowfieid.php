<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "knowfieid".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $Description
 * @property integer $comId
 * @property string $nameRu
 *
 * @property Application[] $applications
 * @property Educationfield[] $educationfields
 * @property Commission $com
 */
class Knowfieid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'knowfieid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'comId', 'nameRu'], 'required'],
            [['Description'], 'string'],
            [['comId'], 'integer'],
            [['Name', 'nameRu'], 'string', 'max' => 100],
            [['comId'], 'exist', 'skipOnError' => true, 'targetClass' => Commission::className(), 'targetAttribute' => ['comId' => 'Id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'comId' => 'Commmission',
            'nameRu' => 'Name Russion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Application::className(), ['knowfieidId' => 'Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducationfields()
    {
        return $this->hasMany(Educationfield::className(), ['KanowId' => 'Id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCom()
    {
        return $this->hasOne(Commission::className(), ['Id' => 'comId']);
    }
}
