<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models;
use dosamigos\datepicker\DatePicker;


?>

<div class="application-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'secondName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'middleName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'birthday')->widget(
        DatePicker::className(), [

        'attribute' => 'date',

        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>


    <?= $form->field($model,'regionId')->dropDownList(
        ArrayHelper::map(models\Regions::find()->all(),'Id','Name'),
        ['prompt'=>'Select Region',
            'onchange'=>'$.post("index.php?r=application/lists&id='.'"+$(this).val(),function(data){
                       $("select#application-districtid").html(data);
                         })                      
                        ']
    )?>
    <?= $form->field($model,'districtId')->dropDownList(
        ArrayHelper::map(models\District::find()->all(),'Id','Name'),
        ['prompt'=>'Select District']
    )?>


    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'phoneNumber')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'universityname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model,'countryId')->dropDownList(
        ArrayHelper::map(models\Countries::find()->all(),'Id','CountryName'),
        ['prompt'=>'Select Country']
    )?>
    <?= $form->field($model, 'specialization')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'docseriya')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'docnumber')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'docregnum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'givendate')->widget(
        DatePicker::className(), [
        // inline too, not bad
        //'inline' => true,
        'attribute' => 'date',
        // modify template for custom rendering
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>
    <?= $form->field($model, 'entrydate')->widget(
        DatePicker::className(), [
        // inline too, not bad
        //'inline' => true,
        'attribute' => 'date',
        // modify template for custom rendering
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>
    <?= $form->field($model, 'enddate')->widget(
        DatePicker::className(), [
        // inline too, not bad
        //'inline' => true,
        'attribute' => 'date',
        // modify template for custom rendering
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model,'eduformId')->dropDownList(
        ArrayHelper::map(models\Eduform::find()->all(),'Id','Name'),
        ['prompt'=>'Select Education form']
    )?>
    <?= $form->field($model,'degreeId')->dropDownList(

        ArrayHelper::map(models\Degree::find()->all(),'Id','Name'),
        ['prompt'=>'Select Degree']
    )?>
    <?= $form->field($model,'knowfieidId')->dropDownList(
        ArrayHelper::map(models\Knowfieid::find()->all(),'Id','Name'),
        ['prompt'=>'Select Know',
            'onchange'=>'$.post("index.php?r=application/listedu&id='.'"+$(this).val(),function(data){
                       $("select#application-educationfieldid").html(data);
                         })                      
                        ']
    )?>
    <?= $form->field($model,'educationfieldId')->dropDownList(
        ArrayHelper::map(models\Educationfield::find()->all(),'Id','Name'),
        ['prompt'=>'Select Education']
    )?>
    <?= $form->field($model,'doctypeId')->dropDownList(
        ArrayHelper::map(models\Documenttyps::find()->all(),'Id','Name'),
        ['prompt'=>'Select Education']
    )?>

    <?= $form->field($model,'fileApplicaton')->fileInput()?>
    <?= $form->field($model,'filePass')->fileInput()?>
    <?= $form->field($model,'fileDiploma')->fileInput()?>
    <?= $form->field($model,'fileAppendix')->fileInput()?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
