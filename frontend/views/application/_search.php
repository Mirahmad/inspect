<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'uniqueId') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'secondName') ?>

    <?= $form->field($model, 'middleName') ?>

    <?php // echo $form->field($model, 'birthday') ?>

    <?php // echo $form->field($model, 'regionId') ?>

    <?php // echo $form->field($model, 'districtId') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phoneNumber') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'universityname') ?>

    <?php // echo $form->field($model, 'countryId') ?>

    <?php // echo $form->field($model, 'specialization') ?>

    <?php // echo $form->field($model, 'docseriya') ?>

    <?php // echo $form->field($model, 'docnumber') ?>

    <?php // echo $form->field($model, 'docregnum') ?>

    <?php // echo $form->field($model, 'givendate') ?>

    <?php // echo $form->field($model, 'entrydate') ?>

    <?php // echo $form->field($model, 'enddate') ?>

    <?php // echo $form->field($model, 'eduformId') ?>

    <?php // echo $form->field($model, 'degreeId') ?>

    <?php // echo $form->field($model, 'knowfieidId') ?>

    <?php // echo $form->field($model, 'educationfieldId') ?>

    <?php // echo $form->field($model, 'doctypeId') ?>

    <?php // echo $form->field($model, 'applications') ?>

    <?php // echo $form->field($model, 'passport') ?>

    <?php // echo $form->field($model, 'diploma') ?>

    <?php // echo $form->field($model, 'appendix') ?>

    <?php // echo $form->field($model, 'statusId') ?>

    <?php // echo $form->field($model, 'comId') ?>

    <?php // echo $form->field($model, 'acceptdate') ?>

    <?php // echo $form->field($model, 'changeddate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
