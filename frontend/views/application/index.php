<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Application', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'uniqueId',
            'name',
            'secondName',
            'middleName',
            //'birthday',
            //'regionId',
            //'districtId',
            //'address:ntext',
            //'phoneNumber',
            //'email:email',
            //'universityname',
            //'countryId',
            //'specialization:ntext',
            //'docseriya',
            //'docnumber',
            //'docregnum',
            //'givendate',
            //'entrydate',
            //'enddate',
            //'eduformId',
            //'degreeId',
            //'knowfieidId',
            //'educationfieldId',
            //'doctypeId',
            //'applications',
            //'passport',
            //'diploma',
            //'appendix',
            //'statusId',
            //'comId',
            //'acceptdate',
            //'changeddate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
