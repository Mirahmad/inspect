<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Application */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'uniqueId',
            'name',
            'secondName',
            'middleName',
            'birthday',
            'regionId',
            'districtId',
            'address:ntext',
            'phoneNumber',
            'email:email',
            'universityname',
            'countryId',
            'specialization:ntext',
            'docseriya',
            'docnumber',
            'docregnum',
            'givendate',
            'entrydate',
            'enddate',
            'eduformId',
            'degreeId',
            'knowfieidId',
            'educationfieldId',
            'doctypeId',
            'applications',
            'passport',
            'diploma',
            'appendix',
            'statusId',
            'comId',
            'acceptdate',
            'changeddate',
        ],
    ]) ?>

</div>
