<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
?>
<body data-spy="scroll" data-target="#navmenu">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class='preloader'><div class='loaded'>&nbsp;</div></div>
<!--Home page style-->


<section id="home" class="home">
    <div class="home-overlay-fluid">
        <div class="container">
            <div class="row">
                <div class="main_slider_area">
                    <div class="slider">
                        <div class="single_slider wow fadeIn" data-wow-duration="2s">
                            <h2>HELLO!</h2>
                            <p>We love our work...</p>
                            <p>And We're taking it seriously...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="register" class="register">
    <div class="container-fullwidth">
        <div class="row text-center">
            <div class="col-sm-6 col-xs-6 no-padding">
                <div class="single_register single_login">
                    <?php
                    echo Html::a('Login', ['site/login']);
                    ?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 no-padding">
                <div class="single_register">
                    <a href="">Register</a>
                </div>
            </div>
        </div>
    </div>
</section>






</body>
