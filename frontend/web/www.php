
<!DOCTYPE html>
<html lang="uz">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-param" content="_csrf-frontend">
    <meta name="csrf-token" content="ei5DVk9vUEY1YhoTAxdoJAJ7HBA8BhoAFkQpLAoXMzQ0GG4MBSpiBA==">
    <title>O'zbekiston Respublikasi Axborot texnologiyalari va kommunikatsiyalarini rivojlantirish vazirligi</title>
    <link href="/assets/3c58d997/css/bootstrap.css" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/media.css" rel="stylesheet"><body>
</head>


<ul class="fix_list">
    <li>
        <a href="/uz/pages/appeal" class="scream"><img src="/img/i1.png" alt=""/></a>
    </li>
    <li>
        <a href="/uz/site/appeal" class="scream"><img src="/img/h2.png" alt=""/></a>
    </li>
    <li>
        <a href="/uz/news/rss" class="scream scream2"><img src="/img/h3.png" alt=""/></a>
    </li>
</ul>








<div class="wrapper">
    <div class="background"></div>
    <div class="paper_boxes">
        <div class="sphere_in">
            <div class="sphere">
                <div class="drive"></div>
                <img src="/img/sphere.png">
            </div>
        </div>

        <div class="head">
            <div class="container nopade relative">

                <!-- Button trigger modal -->
                <button type="button" class="navbar-toggle my_toggle" data-toggle="modal" data-target="#myModal">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="col-md-8 nopade relative ">
                    <div class="col-sm-2 nopade logo_img"><a href="/uz"><img src="/img/logo.png"></a></div>
                    <div class="col-sm-9 nopade logo_name">
                        <a href="/uz" class="logo_text">
                            <span>O'zbekiston Respublikasi Axborot texnologiyalari va kommunikatsiyalarini rivojlantirish vazirligi</span>
                        </a>
                        <p class="beta">Diqqat! Veb-sayt test rejimida ishlamoqda.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 nopaderight relative ">

                    <form  id="search_hold" class="search_form for_form_search"  method="get" action="/uz/search/view">
                        <input id="search_box" class="search_box" type="text" name="search_hold" placeholder="Sayt bo'yicha izlash">
                        <button id="search_btn" class="search_btn" type="submit"></button>
                    </form>

                    <div class="special_views">
                        <a href="#" class="icon_accessibility dataTooltip" data-toggle="dropdown" data-placement="bottom" title="Махсус имкониятлар" aria-expanded="true"><img src="/img/eye.png"></a>
                        <div class="dropdown-menu dropdown-menu-right specialViewArea no-propagation">
                            <!--<div class="triangle2"></div>-->

                            <div class="appearance">
                                <p class="specialTitle">Ko'rinish</p>

                                <div class="squareAppearances">
                                    <div class="squareBox spcNormal" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Оддий кўриниш">A</div>
                                </div>
                                <div class="squareAppearances">
                                    <div class="squareBox spcWhiteAndBlack" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Оқ-қора кўриниш">A</div>
                                </div>
                                <div class="squareAppearances">
                                    <div class="squareBox spcDark" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Қоронғилашган кўриниш">A</div>
                                </div>
                            </div>


                            <div class="appearance">
                                <p class="specialTitle">Shrift o'lchami</p>

                                <div class="block">
                                    <div class="sliderText">Kattalashtirish <span class="range">0 </span>%</div>
                                    <div id="fontSizer" class="defaultSlider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span></div>
                                </div>
                                <div class="block">
                                    <div class="sliderZoom" style="margin-top: 8px"><span class="range">0</span> % Kattalashtirish</div>
                                    <div id="zoomSizer" class="defaultSlider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span></div>
                                </div>
                            </div>

                            <div class="more_margin"></div>
                            <div class="appearance">
                                <div class="pull-right">
                                    <p id="narratorHelp" class="rounded pointer" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Помощь"></p>
                                </div>
                                <p class="specialTitle">Ekran suxandoni</p>


                                <div class="block">
                                    <div class="blueCheckbox">
                                        <input type="checkbox" id="speechSwitcher" title="Функция экранный диктор включена" name="speechSwitcher" />
                                        <label for="speechSwitcher"><span></span>Yoqish / O'chirish</label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <ul class="lang">
                        <li class="active"><a href="http://mitc.uz/uz">Uz</a></li..><li><a href="http://mitc.uz/ru">Ру</a></li..><li><a href="http://mitc.uz/en">En</a></li..></ul>
                    <div class="call_box">
                        <div>Ishonch telefonlari</div>
                        <span>0-800-200-41-07</span>
                        <span>11-44</span><br>
                        <a href="/uz/pages/phone_numbers">Batafsil</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="head_menu">
                    <nav id="w0" class="menu navbar" role="navigation"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse"><span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span></button></div><div id="w0-collapse" class="collapse navbar-collapse"><ul id="w1" class="navbar-nav nav"><li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Vazirlik haqida <b class="caret"></b></a><ul id="w2" class="dropdown-menu"><li class=""><a href="/uz/pages/about_ministry" target="" tabindex="-1">Vazirlik haqida</a></li>
                                            <li class=""><a href="/uz/pages/board" target="" tabindex="-1">Hay`at</a></li>
                                            <li class=""><a href="/uz/management/index" target="" tabindex="-1">Rahbariyat</a></li>
                                            <li class=""><a href="/uz/pages/ministry_structure" target="" tabindex="-1">Vazirlik tuzilmasi</a></li>
                                            <li class=""><a href="/uz/management/head" target="" tabindex="-1">Markaziy apparat</a></li>
                                            <li class=""><a href="/uz/management/subordinate" target="" tabindex="-1">Hududiy boshqarmalar</a></li>
                                            <li class=""><a href="/uz/pages/Offers_ministry" target="" tabindex="-1">Vazirlikka murojaat</a></li>
                                            <li class=""><a href="/uz/management/system" target="" tabindex="-1">Tizimdagi tashkilotlar</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Axborot xizmati <b class="caret"></b></a><ul id="w3" class="dropdown-menu"><li class=""><a href="/uz/news/category/1" target="" tabindex="-1">Vazirlik yangiliklari</a></li>
                                            <li class=""><a href="/uz/news/category/2" target="" tabindex="-1">OAV AKT haqida</a></li>
                                            <li class=""><a href="/uz/news/category/4" target="" tabindex="-1">Аnons</a></li>

                                            <li class=""><a href="/uz/news/accreditation" target="" tabindex="-1">OAV vakillarini akkreditasiyadan o&#039;tkazish</a></li>
                                            <li class=""><a href="/uz/pages/press_contact" target="" tabindex="-1">Axborot xizmati bilan bog&#039;lanish</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Faoliyat <b class="caret"></b></a><ul id="w4" class="dropdown-menu"><li class=""><a href="/uz/pages/communication" target="" tabindex="-1">Telekommunikatsiya</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                            <li class=""><a href="/uz/pages/actions_strategy" target="" tabindex="-1">Harakatlar strategiyasi</a></li>
                                            <li class=""><a href="/uz/pages/information_technologies" target="" tabindex="-1">Axborot texnologiyalari</a></li>
                                            <li class=""><a href="/uz/pages/post" target="" tabindex="-1">Pochta</a></li>
                                            <li class=""><a href="/uz/pages/ict_education" target="" tabindex="-1">AKTda ta&#039;lim</a></li>
                                            <li class=""><a href="/uz/pages/international_relations" target="" tabindex="-1">Xalqaro aloqalar</a></li>
                                            <li class=""><a href="/uz/pages/council_orders" target="" tabindex="-1">Jamoatchilik maslahat kengashi</a></li>
                                            <li class=""><a href="/uz/pages/e_reestr" target="" tabindex="-1">Davlat xizmatlari reestri</a></li>
                                            <li class=""><a href="/uz/pages/about_fond" target="" tabindex="-1">AKT ni rivojlantrish jamg`armasi</a></li>
                                            <li class=""><a href="/uz/pages/reports_of_ministry" target="" tabindex="-1">Hisobotlar</a></li>
                                            <li class=""><a href="/uz/pages/anti-corruption" target="" tabindex="-1">Korrupsiyaga qarshi kurash</a></li>
                                            <li class=""><a href="/uz/pages/inves_tory" target="" tabindex="-1">AKT ga investitsiyalar</a></li>
                                            <li class=""><a href="/uz/pages/list_of_services" target="" tabindex="-1">Interaktiv davlat xizmatlari ro&#039;yxati</a></li>
                                            <li class=""><a href="/uz/pages/cooperation_with_other_org" target="" tabindex="-1">Boshqa tashkilotlar bilan hamkorlik</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Hujjatlar <b class="caret"></b></a><ul id="w5" class="dropdown-menu"><li class=""><a href="/uz/pages/Documents/615" target="" tabindex="-1">Qonunlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/623" target="" tabindex="-1">Prezident qarorlari va farmonlari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/622" target="" tabindex="-1">Hukumat qarori va farmoyishlari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/1208" target="" tabindex="-1">Tadbirkorlikka oid hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/621" target="" tabindex="-1">Qo‘shma qarorlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/620" target="" tabindex="-1">Davlat standarti</a></li>
                                            <li class=""><a href="/uz/pages/Documents/618" target="" tabindex="-1">Adliya vazirligi tomonidan ro‘yxatga olingan me’yoriy hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/regulation" target="" tabindex="-1">Me’yoriy hujjatlar loyihalari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/616" target="" tabindex="-1">Kuchini yo‘qotgan me’yoriy hujjatlar</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Boshqaruv <b class="caret"></b></a><ul id="w6" class="dropdown-menu"><li class=""><a href="/uz/pages/regulation/489" target="" tabindex="-1">Davlat nazorati</a></li>
                                            <li class=""><a href="/uz/pages/regulation/627" target="" tabindex="-1">Litsenziyalash</a></li>
                                            <li class=""><a href="/uz/pages/regulation/626" target="" tabindex="-1">Sertifikatlashtirish</a></li>
                                            <li class=""><a href="/uz/pages/regulation/625" target="" tabindex="-1">Standartlashtirish</a></li>
                                            <li class=""><a href="/uz/pages/communication/553" target="" tabindex="-1">Radiochastota spektridan foydalanishni boshqarish</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/pages/egovernment/3038" target="" data-toggle="dropdown">Elektron hukumat <b class="caret"></b></a><ul id="w7" class="dropdown-menu"><li class=""><a href="/uz/pages/egovernment/3034" target="" tabindex="-1">Elektron hukumatga oid me&#039;yoriy-huquqiy hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3035" target="" tabindex="-1">Elektron hukumat bo&#039;yicha loyihalar</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/2074" target="" tabindex="-1">O&#039;zbekistonda elektron hukumatni joriy etish va rivojlantirish holati</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3038" target="" tabindex="-1">&quot;Elektron Hukumat&quot; tuzilmasi</a></li></ul></li>
                                    <li class=""><a href="/uz/pages/contacts" target="">Bog&#039;lanish</a></li></ul></div></div></nav></div>

            </div>
        </div>

        <div class="news_box">
            <div class="container nopade relative">
                <div class="col-md-9 nopadeleft">

                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Vazirlik yangiliklari</a></li>
                            <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">OAV AKT haqida</a></li>
                            <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Anonslar</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <div class="pro_news">


                                    <div class="col-sm-5 nopade img_block">
                                        <img align="left" width="auto" style="margin:10px; height:239px" src="http://mitc.uz/media/6ec4edca-4aff-d9e7-f611-4f000c65c669.jpg" ></div>
                                    <div class="col-sm-7 title_marg">
                                        <a href="/uz/news/view/483" class="title">Innovatsiya markazi ko‘magida mahalliy IT-kompaniyalar xalqaro ko‘rgazmada ishtirok etadilar</a>
                                        <div class="anons_box">Milliy dasturiy ta&rsquo;minot mahsulotlari ishlab chiqaruvchilarini qo&lsquo;llab-quvvatlash maqsadida tashkil qilingan &laquo;Mirzo Ulugbek Innovation Center&raquo; Innovatsiya markazi ko&lsquo;magida yurtimizdagi IT-kompaniyalar Hindistonning nufuzli &laquo;INDIASOFT 2018&raquo; ko&lsquo;rgazmasida ishtirok etish imkoniga ega bo&lsquo;ladilar.&nbsp;
                                        </div>
                                        <div class="mini_way">
                                            <div class="date_box">21.12.2017</div>
                                            <span>|</span>
                                            <span class="eyes"><img src="/img/eye2.png" alt="">158</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <ul class="news_list">

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/480"
                                               class="title">
                                                «Best Soft Challenge-2017» tanlovi g‘oliblari e'lon qilindi
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">18.12.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                320                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/479"
                                               class="title">
                                                TATU va KOICA istiqbolli loyihani amalga oshiradi
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">14.12.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                558                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/478"
                                               class="title">
                                                Yerevanda radiochastotadan samarali foydalanish muhokama qilinmoqda
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">14.12.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                386                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                    </ul>
                                    <div class='clearfix'></div>
                                    <div><a href="/uz/news/index">Barcha yangiliklar</a></div><br>

                                    <div class="centre"><a href="/uz/news/262"><img src="/img/banner_uz.png" alt=""></a></div>
                                </div>


                            </div>

                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <div class="pro_news">


                                    <div class="col-sm-5 nopade img_block">
                                        <img align="left" width="auto" style="margin:10px; height:239px" src="http://mitc.uz/media/2d7b408d-96d0-c74e-f638-c726e90ee601.jpg" ></div>
                                    <div class="col-sm-7 title_marg">
                                        <a href="/uz/news/view/335" class="title">Axborot jamiyati: istiqbol va imkoniyatlar</a>
                                        <div class="anons_box">Poytaxtimizdagi Xalqaro press-klubda O&lsquo;zbekiston Respublikasi Axborot texnologiyalari va kommunikatsiyalarini rivojlantirish vazirligi tomonidan shu mavzuga bag&lsquo;ishlangan matbot anjumani o&lsquo;tkazildi. Unda mamlakatimizda axborot-kommunikatsiya texnologiyalari, internet tarmog&lsquo;ining milliy bo&lsquo;g&lsquo;ini, mobil va telekommunikatsiya ...</div>
                                        <div class="mini_way">
                                            <div class="date_box">18.05.2017</div>
                                            <span>|</span>
                                            <span class="eyes"><img src="/img/eye2.png" alt="">4135</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <ul class="news_list">

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/296"
                                               class="title">
                                                «Elektron hukumat» — xalq bilan muloqotning zamonaviy mexanizmi
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">09.03.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                3264                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/292"
                                               class="title">
                                                Har bir murojaat e’tiborda
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">03.03.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                3053                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                    </ul>
                                    <div class='clearfix'></div>
                                    <div><a href="/uz/news/index">Barcha yangiliklar</a></div><br>

                                    <div class="centre"><a href="/uz/news/262"><img src="/img/banner_uz.png" alt=""></a></div>
                                </div>


                            </div>

                            <div role="tabpanel" class="tab-pane" id="tab3">
                                <div class="pro_news">


                                    <div class="col-sm-5 nopade img_block">
                                        <img align="left" width="auto" style="margin:10px; height:239px" src="http://mitc.uz/media/f959c2ad-622c-5555-e5f0-c00c7a9edcb7.jpg" ></div>
                                    <div class="col-sm-7 title_marg">
                                        <a href="/uz/news/view/470" class="title">Kommutatsiya va server uskunalari jihozlarini sug‘urtalash bo'yicha tanlov e’lon qilinadi</a>
                                        <div class="anons_box">Vazirlik Axborot tizimlarini tatqiq etish markazi tasarrufidagi elektrotexnika (chet elda ishlab chiqarilgan kommutatsiya va server uskunalari) jihozlarini sug&lsquo;urtalash maqsadida tanlov e&rsquo;lon qilinadi. Sug&lsquo;urta uchun to&lsquo;lanadigan maksimal mukofot haqi 125000 ...</div>
                                        <div class="mini_way">
                                            <div class="date_box">04.12.2017</div>
                                            <span>|</span>
                                            <span class="eyes"><img src="/img/eye2.png" alt="">301</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <ul class="news_list">

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/468"
                                               class="title">
                                                Vazirlik 2017 yil uchun moliyaviy-xo‘jalik faoliyatini auditorlik tekshiruvidan o‘tkazish maqsadida tanlov e’lon qiladi
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">04.12.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                272                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/405"
                                               class="title">
                                                ICTWEEK Uzbekistan 2017 Matbuot-anjumani
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">11.09.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                1907                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                        <li class="col-sm-4 nopadeleft">
                                            <a href="/uz/news/view/394"
                                               class="title">
                                                18-22 sentabr kunlari "ICTWEEK-2017" haftaligi bo‘lib o‘tadi
                                            </a>
                                            <div class="mini_way">
                                                <div
                                                        class="date_box">24.08.2017</div>
                                                <span>|</span>
                                                <span class="eyes">
                                <img src="/img/eye2.png" alt="">
                                2532                            </span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>

                                    </ul>
                                    <div class='clearfix'></div>
                                    <div><a href="/uz/news/index">Barcha yangiliklar</a></div><br>

                                    <div class="centre"><a href="/uz/news/262"><img src="/img/banner_uz.png" alt=""></a></div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 nopaderight">

                    <div class="event_name">
                        <!--    --></div>
                    <div class="event_box">
                        <img src="/img/ddgov_uz.jpg">
                        <a href="http://lex.uz/pages/getpage.aspx?lact_id=3107036#3109519" target="_blank" class="event_text"><i></i>
                            <span></span>
                        </a>

                    </div>
                    <!--    -->
                    <div class="white_menu">
                        <ul class="link_list" id="xscroll_in">
                            <li><img src="/img/mail.png"><a href="http://murojaat.mitc.uz/uz">Vazir virtual qabulxonasi</a></li>
                            <li><img src="/img/sovet.png"><a href="/uz/pages/council_orders">Jamoatchilik maslahat kengashi</a></li>
                            <li><img src="/img/vacancy.png"><a href="https://my.gov.uz/uz/tools/vacancies?authority=48">Bo'sh ish o'rinlari</a></li>
                            <li><img src="/img/tender.png"><a href="/uz/pages/tender_anons">Tender e’lonlari</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mini_slide">
                        <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <a href="http://mitc.uz/uz/news/announce" target="_blank">
                                        <img src="/media/3a431880-cf3a-5146-c0ae-adb7ed9b68da.jpg" alt="">
                                        <div class="item_text"><div class="text"><span></span>
                                                <!--                                -->                            </div></div>
                                    </a>
                                </div>
                                <div class="item ">
                                    <a href="http://ictweek.uz/" target="_blank">
                                        <img src="/media/c8db5af3-5ee4-4902-b0a5-99c8550b9a4c.jpg" alt="">
                                        <div class="item_text"><div class="text"><span></span>
                                                <!--                                -->                            </div></div>
                                    </a>
                                </div>
                                <div class="item ">
                                    <a href="http://mustaqillik.uz/uz" target="_blank">
                                        <img src="/media/6af43aa2-50a9-f533-082c-80a7a27d1b87.jpg" alt="">
                                        <div class="item_text"><div class="text"><span></span>
                                                <!--                                -->                            </div></div>
                                    </a>
                                </div>
                                <div class="item ">
                                    <a href="http://mitc.uz/uz/news/262" target="_blank">
                                        <img src="/media/ae1d110d-bb29-aeca-361b-337ec3a2d51c.jpg" alt="">
                                        <div class="item_text"><div class="text"><span></span>
                                                <!--                                -->                            </div></div>
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="activities">
                    <div class="mini_title">Faoliyat yo'nalishlari</div>
                    <ul class="active_menu">
                        <li>
                            <a href="/uz/pages/communication">
                                <span class="parent_box">
                                    <span class="ibox">
                                        <img src="/img/r1.png" alt="">
                                    </span>
                                </span>
                                <span class="text">Telekommunikatsiya</span>
                            </a>
                        </li>
                        <li>
                            <a href="/uz/pages/actions_strategy">
                                <span class="parent_box brow">
                                    <span class="ibox">
                                        <img src="/img/tegy.png" alt="">
                                    </span>
                                </span>
                                <span class="text">Harakatlar strategiyasi</span>
                            </a>
                        </li>
                        <li>
                            <a href="/uz/pages/information_technologies">
                                <span class="parent_box red">
                                    <span class="ibox">
                                        <img src="/img/r2.png" alt="">
                                    </span>
                                </span>
                                <span class="text">Axborot texnologiyalari</span>
                            </a>
                        </li>
                        <li>
                            <a href="/uz/pages/post">
                                <span class="parent_box green">
                                    <span class="ibox">
                                        <img src="/img/r3.png" alt="">
                                    </span>
                                </span>
                                <span class="text">Pochta</span>
                            </a>
                        </li>
                        <li>
                            <a href="/uz/pages/egovernment/3037">
                                <span class="parent_box violet">
                                    <span class="ibox">
                                        <img src="/img/r4.png" alt="">
                                    </span>
                                </span>
                                <span class="text">Elektron hukumat</span>
                            </a>
                        </li>
                        <li>
                            <a href="/uz/pages/inves_tory">
                                <span class="parent_box brown">
                                    <span class="ibox">
                                        <img src="/img/r8.png" alt="">
                                    </span>
                                </span>
                                <span class="text">AKT <span style="text-transform: lowercase">ga</span> investitsiyalar</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mini_title">Interaktiv xizmatlar</div>
                <ul class="interactive_services">
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/pages/appeal">
                                <img src="/img/i1.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/pages/appeal">
                                Murojaat            </a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="https://my.gov.uz/ru/service/138/48">
                                <img src="/img/i2.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="https://my.gov.uz/ru/service/138/48">
                                Litsenziyalash            </a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/service/view" >
                                <img src="/img/i3.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/service/view">
                                Radiochastotalar jadvali            </a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/pages/registry_interactive_services_industry" >
                                <img src="/img/i4.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/pages/registry_interactive_services_industry" >
                                Soha interaktiv xizmat reestori            </a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="http://rc-service.unicon.uz/?page=products_rsa" >
                                <img src="/img/i5.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="http://rc-service.unicon.uz/?page=products_rsa">Elektron raqamli imzoga ariza</a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/pages/electronic_payments" >
                                <img src="/img/i6.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/pages/electronic_payments">
                                Elektron to'lovlar</a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/pages/e_services">
                                <img src="/img/i7.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/pages/e_services">
                                Elektron hisobot</a>
                        </div>
                    </li>
                    <li>
                        <div class="sphere_link">
                            <a href="/uz/service/prefix">
                                <img src="/img/i8.png" alt="">
                            </a>
                        </div>
                        <div class="vert_link">
                            <a href="/uz/service/prefix">Telefon prefikslari</a>
                        </div>
                    </li>
                </ul>
                <div class="mini_title">Ko'rsatkichlar</div>
                <ul class="charts_list">

                    <li>
                        <a href="/uz/stat/stats" class="chart_hover">
                            <div class="chart_parent">
                                <div id="d1" class="chart"></div>
                                <div class="total"><span>
                1845            </span></div>
                                <div class="text"><a href="/uz/stat/stats">Vazirlikka qilingan murojaatlar statistikasi</a></div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="/uz/stat/4" class="chart_hover">
                            <div class="chart_parent">
                                <div id="d2" class="chart"></div>
                                <div class="total"><span>14756.9</span></div>
                                <div class="text"><a href="/uz/stat/4">Jami internetdan foydalanuvchilar soni (ming)</a></div>

                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/uz/stat/2" class="chart_hover">
                            <div class="chart_parent">
                                <div id="d3" class="chart"></div>
                                <div class="total"><span>65734</span></div>
                                <div class="text"><a href="/uz/stat/2">Xalqaro axborot tarmoqlaridan foydalanish umumiy tezligi (Mbit/s)</a></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="chart_hover" href="/uz/stat/9">
                            <div class="chart_parent">
                                <div id="d4" class="chart"></div>
                                <div class="total"><span>1720.2</span></div>
                                <div class="text"><a href="/uz/stat/9">Amaldagi ERI kalitlari umumiy soni (dona.)</a></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a  class="chart_hover" href="/uz/stat/11">
                            <div class="chart_parent">
                                <div id="d5" class="chart"></div>
                                <div class="total"><span>95%</span></div>
                                <div class="text"><a href="/uz/stat/11">Aholini raqamli televidenie bilan qamrab olinishi darajasi (%)</a></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="mini_title">So'rovnoma</div>
                <div class="questionnaire">
                    <div class="col-md-5">
                        <div class="question">Uyda internetga ulanganmisiz?</div>
                        <ul class="answer_list">
                            <li>
                                <span>Ha</span>
                                <input type="radio" class="" name="answer-option" value="0">
                            </li>
                            <li>
                                <span>Yo'q</span>
                                <input type="radio" class="" name="answer-option" value="1">
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-5">
                        <ul class="links_list">
                            <li><a href="/uz/site/vote" class="answers">Ovoz berish</a></li>
                            <li><a href="/uz/site/vote">Natijalar</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <div class="online">
                            Onlayn foydalanuvchilar: <span>
<!--                -->                                69            </span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>













<div class="mapper">
    <div class="container nopade relative">
        <div class="col-md-6 relative nopadeleft just_mini_list">
            <div class="list_name">Hududiy boshqarmalar</div>

            <div class="mini_map">
                <div class="map_bg"></div>
                <div id="vmap" style="width: 1100px; height: 820px; " class="vmapper"></div>
                <div id="mapLabels"></div>
            </div>

        </div>
        <div class="col-md-6 relative nopaderight ">
            <div class="list_name">Foydali havolalar</div>
            <ul class="mini_list">
                <li class="col-md-6"><img src="/img/press.png"><a href="http://www.press-service.uz/"><span>O’ZBEKISTON RESPUBLIKASI PREZIDENTI MATBUOT XIZMATI</span></a></li>
                <li class="col-md-6"><img src="/img/high.png"><a href="http://parliament.gov.uz/"><span>O’ZBEKISTON RESPUBLIKASI OLIY MAJLISI QONUNCHILIK PALATASI</span></a></li>
                <li class="col-md-6"><img src="/img/portal.png"><a href="http://senat.gov.uz/"><span>O'ZBEKISTON RESPUBLIKASI OLIY MAJLISINING SENATI</span></a></li>
                <li class="col-md-6"><img src="/img/senat.png"><a href="https://www.gov.uz"><span>O’ZBEKISTON RESPUBLIKASI HUKUMATI PORTALI</span></a></li>

            </ul>
            <div class="clearfix"></div>
            <div class="list_name">LOYIHALAR</div>
            <ul class="mini_list menu_list2">
                <li class="col-md-6"><img src="/img/www2.png"><a href="http://www.uz/"><span>Milliy qidiruv tizimi</span></a></li>
                <li class="col-md-6"><img src="/img/ziyo.png"><a href="http://ziyonet.uz/"><span>Ta'lim portali</span></a></li>
                <li class="col-md-6"><img src="/img/gov_uz.png"><a href="https://my.gov.uz"><span>Yagona interaktiv davlat xizmatlari portali</span></a></li>
                <li class="col-md-6"><img src="/img/developer.png"><a href="http://software.uz/"><span>Dasturchilar va dasturiy mahsulotlar milliy katalogi</span></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>        </div>
</div>

<div class="clearfix">


</div>

<div class="list_way">
    <div class="container nopade relative">
        <a href="#" class="map_btn"><span></span>Sayt xaritasi</a>
    </div>
    <div>
        <div class="footer_list">
            <div class="list_in">


                <div class="col-md-x-save"><ul class="list-unstyled">
                        <li ><a href = "/uz" >Asosiy</a ></li >
                        <li ><a target = "_blank" href = "http://uforum.uz/forumdisplay.php?f=297" >Forum</a ></li >
                        <li ><a href = "#" >Obuna</a ></li >
                    </ul><ul class="list-unstyled">
                        <li>Vazirlik haqida<li><a href="/uz/pages/about_ministry">Vazirlik haqida</a></li><li><a href="/uz/pages/board">Hay`at</a></li><li><a href="/uz/management/index">Rahbariyat</a></li><li><a href="/uz/pages/ministry_structure">Vazirlik tuzilmasi</a></li><li><a href="/uz/management/head">Markaziy apparat</a></li><li><a href="/uz/management/subordinate">Hududiy boshqarmalar</a></li><li><a href="/uz/pages/Offers_ministry">Vazirlikka murojaat</a></li><li><a href="/uz/management/system">Tizimdagi tashkilotlar</a></li></li></ul></div> <div class="col-md-x-save"><ul class="list-unstyled">
                        <li>Axborot xizmati<li><a href="/uz/news/category/1">Vazirlik yangiliklari</a></li><li><a href="/uz/news/category/2">OAV AKT haqida</a></li><li><a href="/uz/news/category/4">Аnons</a></li><li><a href="/uz/news/category/5">E'lonlar</a></li><li><a href="/uz/pages/speeches_statements">Rahbariyatning chiqish va bayonotlari</a></li><li><a href="/uz/pages/publications_communications_informatization">Soha nashrlari</a></li><li><a href="/uz/pages/publications">Nashrlar</a></li><li><a href="/uz/news/vfiles">Videolavhalar</a></li><li><a href="/uz/pages/major_events">Мatbuot anjumanlari jadvali</a></li><li><a href="/uz/pages/media_plan">Media-reja</a></li><li><a href="/uz/news/accreditation">OAV vakillarini akkreditasiyadan o'tkazish</a></li><li><a href="/uz/pages/press_contact">Axborot xizmati bilan bog'lanish</a></li></li></ul></div> <div class="col-md-x-save"><ul class="list-unstyled">
                        <li>Faoliyat<li><a href="/uz/pages/communication">Telekommunikatsiya</a></li><li><a href="/uz/pages/egovernment/3037">"Elektron hukumat" tizimi</a></li><li><a href="/uz/pages/actions_strategy">Harakatlar strategiyasi</a></li><li><a href="/uz/pages/information_technologies">Axborot texnologiyalari</a></li><li><a href="/uz/pages/post">Pochta</a></li><li><a href="/uz/pages/ict_education">AKTda ta'lim</a></li><li><a href="/uz/pages/international_relations">Xalqaro aloqalar</a></li><li><a href="/uz/pages/council_orders">Jamoatchilik maslahat kengashi</a></li><li><a href="/uz/pages/e_reestr">Davlat xizmatlari reestri</a></li><li><a href="/uz/pages/about_fond">AKT ni rivojlantrish jamg`armasi</a></li><li><a href="/uz/pages/reports_of_ministry">Hisobotlar</a></li><li><a href="/uz/pages/anti-corruption">Korrupsiyaga qarshi kurash</a></li><li><a href="/uz/pages/inves_tory">AKT ga investitsiyalar</a></li><li><a href="/uz/pages/list_of_services">Interaktiv davlat xizmatlari ro'yxati</a></li><li><a href="/uz/pages/cooperation_with_other_org">Boshqa tashkilotlar bilan hamkorlik</a></li></li></ul></div> <div class="col-md-x-save"><ul class="list-unstyled">
                        <li>Hujjatlar<li><a href="/uz/pages/Documents/615">Qonunlar</a></li><li><a href="/uz/pages/Documents/623">Prezident qarorlari va farmonlari</a></li><li><a href="/uz/pages/Documents/622">Hukumat qarori va farmoyishlari</a></li><li><a href="/uz/pages/Documents/1208">Tadbirkorlikka oid hujjatlar</a></li><li><a href="/uz/pages/Documents/621">Qo‘shma qarorlar</a></li><li><a href="/uz/pages/Documents/620">Davlat standarti</a></li><li><a href="/uz/pages/Documents/618">Adliya vazirligi tomonidan ro‘yxatga olingan me’yoriy hujjatlar</a></li><li><a href="/uz/pages/regulation">Me’yoriy hujjatlar loyihalari</a></li><li><a href="/uz/pages/Documents/616">Kuchini yo‘qotgan me’yoriy hujjatlar</a></li></li></ul></div> <div class="col-md-x-save"><ul class="list-unstyled">
                        <li>Boshqaruv<li><a href="/uz/pages/regulation/489">Davlat nazorati</a></li><li><a href="/uz/pages/regulation/627">Litsenziyalash</a></li><li><a href="/uz/pages/regulation/626">Sertifikatlashtirish</a></li><li><a href="/uz/pages/regulation/625">Standartlashtirish</a></li><li><a href="/uz/pages/communication/553">Radiochastota spektridan foydalanishni boshqarish</a></li></li></ul></div> <div class="col-md-x-save"><ul class="list-unstyled">
                        <li>Elektron hukumat<li><a href="/uz/pages/egovernment/3034">Elektron hukumatga oid me'yoriy-huquqiy hujjatlar</a></li><li><a href="/uz/pages/egovernment/3037">"Elektron hukumat" tizimi</a></li><li><a href="/uz/pages/egovernment/3035">Elektron hukumat bo'yicha loyihalar</a></li><li><a href="/uz/pages/egovernment/2074">O'zbekistonda elektron hukumatni joriy etish va rivojlantirish holati</a></li><li><a href="/uz/pages/egovernment/3038">"Elektron Hukumat" tuzilmasi</a></li></li></ul></div><li><a href="/uz/pages/contacts">Bog'lanish</a></li></ul></div>            </div>
        <div class="clearfix"></div>
    </div>
</div>


</div>
<div class="clearfix"></div>
<div class="footer">
    <div class="container nopade relative">

        <div class="head_menu">
            <nav id="w9" class="menu navbar" role="navigation"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w9-collapse"><span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span></button></div><div id="w9-collapse" class="collapse navbar-collapse"><ul id="w10" class="navbar-nav nav"><li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Vazirlik haqida <b class="caret"></b></a><ul id="w11" class="dropdown-menu"><li class=""><a href="/uz/pages/about_ministry" target="" tabindex="-1">Vazirlik haqida</a></li>
                                    <li class=""><a href="/uz/pages/board" target="" tabindex="-1">Hay`at</a></li>
                                    <li class=""><a href="/uz/management/index" target="" tabindex="-1">Rahbariyat</a></li>
                                    <li class=""><a href="/uz/pages/ministry_structure" target="" tabindex="-1">Vazirlik tuzilmasi</a></li>
                                    <li class=""><a href="/uz/management/head" target="" tabindex="-1">Markaziy apparat</a></li>
                                    <li class=""><a href="/uz/management/subordinate" target="" tabindex="-1">Hududiy boshqarmalar</a></li>
                                    <li class=""><a href="/uz/pages/Offers_ministry" target="" tabindex="-1">Vazirlikka murojaat</a></li>
                                    <li class=""><a href="/uz/management/system" target="" tabindex="-1">Tizimdagi tashkilotlar</a></li></ul></li>
                            <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Axborot xizmati <b class="caret"></b></a><ul id="w12" class="dropdown-menu"><li class=""><a href="/uz/news/category/1" target="" tabindex="-1">Vazirlik yangiliklari</a></li>
                                    <li class=""><a href="/uz/news/category/2" target="" tabindex="-1">OAV AKT haqida</a></li>
                                    <li class=""><a href="/uz/news/category/4" target="" tabindex="-1">Аnons</a></li>
                                    <li class=""><a href="/uz/news/category/5" target="" tabindex="-1">E&#039;lonlar</a></li>
                                    <li class=""><a href="/uz/pages/speeches_statements" target="" tabindex="-1">Rahbariyatning chiqish va bayonotlari</a></li>
                                    <li class=""><a href="/uz/pages/publications_communications_informatization" target="" tabindex="-1">Soha nashrlari</a></li>
                                    <li class=""><a href="/uz/pages/publications" target="" tabindex="-1">Nashrlar</a></li>
                                    <li class=""><a href="/uz/news/vfiles" target="" tabindex="-1">Videolavhalar</a></li>
                                    <li class=""><a href="/uz/pages/major_events" target="" tabindex="-1">Мatbuot anjumanlari jadvali</a></li>
                                    <li class=""><a href="/uz/pages/media_plan" target="" tabindex="-1">Media-reja</a></li>
                                    <li class=""><a href="/uz/news/accreditation" target="" tabindex="-1">OAV vakillarini akkreditasiyadan o&#039;tkazish</a></li>
                                    <li class=""><a href="/uz/pages/press_contact" target="" tabindex="-1">Axborot xizmati bilan bog&#039;lanish</a></li></ul></li>
                            <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Faoliyat <b class="caret"></b></a><ul id="w13" class="dropdown-menu"><li class=""><a href="/uz/pages/communication" target="" tabindex="-1">Telekommunikatsiya</a></li>
                                    <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                    <li class=""><a href="/uz/pages/actions_strategy" target="" tabindex="-1">Harakatlar strategiyasi</a></li>
                                    <li class=""><a href="/uz/pages/information_technologies" target="" tabindex="-1">Axborot texnologiyalari</a></li>
                                    <li class=""><a href="/uz/pages/post" target="" tabindex="-1">Pochta</a></li>
                                    <li class=""><a href="/uz/pages/ict_education" target="" tabindex="-1">AKTda ta&#039;lim</a></li>
                                    <li class=""><a href="/uz/pages/international_relations" target="" tabindex="-1">Xalqaro aloqalar</a></li>
                                    <li class=""><a href="/uz/pages/council_orders" target="" tabindex="-1">Jamoatchilik maslahat kengashi</a></li>
                                    <li class=""><a href="/uz/pages/e_reestr" target="" tabindex="-1">Davlat xizmatlari reestri</a></li>
                                    <li class=""><a href="/uz/pages/about_fond" target="" tabindex="-1">AKT ni rivojlantrish jamg`armasi</a></li>
                                    <li class=""><a href="/uz/pages/reports_of_ministry" target="" tabindex="-1">Hisobotlar</a></li>
                                    <li class=""><a href="/uz/pages/anti-corruption" target="" tabindex="-1">Korrupsiyaga qarshi kurash</a></li>
                                    <li class=""><a href="/uz/pages/inves_tory" target="" tabindex="-1">AKT ga investitsiyalar</a></li>
                                    <li class=""><a href="/uz/pages/list_of_services" target="" tabindex="-1">Interaktiv davlat xizmatlari ro&#039;yxati</a></li>
                                    <li class=""><a href="/uz/pages/cooperation_with_other_org" target="" tabindex="-1">Boshqa tashkilotlar bilan hamkorlik</a></li></ul></li>
                            <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Hujjatlar <b class="caret"></b></a><ul id="w14" class="dropdown-menu"><li class=""><a href="/uz/pages/Documents/615" target="" tabindex="-1">Qonunlar</a></li>
                                    <li class=""><a href="/uz/pages/Documents/623" target="" tabindex="-1">Prezident qarorlari va farmonlari</a></li>
                                    <li class=""><a href="/uz/pages/Documents/622" target="" tabindex="-1">Hukumat qarori va farmoyishlari</a></li>
                                    <li class=""><a href="/uz/pages/Documents/1208" target="" tabindex="-1">Tadbirkorlikka oid hujjatlar</a></li>
                                    <li class=""><a href="/uz/pages/Documents/621" target="" tabindex="-1">Qo‘shma qarorlar</a></li>
                                    <li class=""><a href="/uz/pages/Documents/620" target="" tabindex="-1">Davlat standarti</a></li>
                                    <li class=""><a href="/uz/pages/Documents/618" target="" tabindex="-1">Adliya vazirligi tomonidan ro‘yxatga olingan me’yoriy hujjatlar</a></li>
                                    <li class=""><a href="/uz/pages/regulation" target="" tabindex="-1">Me’yoriy hujjatlar loyihalari</a></li>
                                    <li class=""><a href="/uz/pages/Documents/616" target="" tabindex="-1">Kuchini yo‘qotgan me’yoriy hujjatlar</a></li></ul></li>
                            <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Boshqaruv <b class="caret"></b></a><ul id="w15" class="dropdown-menu"><li class=""><a href="/uz/pages/regulation/489" target="" tabindex="-1">Davlat nazorati</a></li>
                                    <li class=""><a href="/uz/pages/regulation/627" target="" tabindex="-1">Litsenziyalash</a></li>
                                    <li class=""><a href="/uz/pages/regulation/626" target="" tabindex="-1">Sertifikatlashtirish</a></li>
                                    <li class=""><a href="/uz/pages/regulation/625" target="" tabindex="-1">Standartlashtirish</a></li>
                                    <li class=""><a href="/uz/pages/communication/553" target="" tabindex="-1">Radiochastota spektridan foydalanishni boshqarish</a></li></ul></li>
                            <li class="dropdown"><a class="dropdown-toggle" href="/uz/pages/egovernment/3038" target="" data-toggle="dropdown">Elektron hukumat <b class="caret"></b></a><ul id="w16" class="dropdown-menu"><li class=""><a href="/uz/pages/egovernment/3034" target="" tabindex="-1">Elektron hukumatga oid me&#039;yoriy-huquqiy hujjatlar</a></li>
                                    <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                    <li class=""><a href="/uz/pages/egovernment/3035" target="" tabindex="-1">Elektron hukumat bo&#039;yicha loyihalar</a></li>
                                    <li class=""><a href="/uz/pages/egovernment/2074" target="" tabindex="-1">O&#039;zbekistonda elektron hukumatni joriy etish va rivojlantirish holati</a></li>
                                    <li class=""><a href="/uz/pages/egovernment/3038" target="" tabindex="-1">&quot;Elektron Hukumat&quot; tuzilmasi</a></li></ul></li>
                            <li class=""><a href="/uz/pages/contacts" target="">Bog&#039;lanish</a></li></ul></div></div></nav></div>

        <div class="clearfix"></div>

        <div class="col-sm-4 nopadeleft foot_text">
            O'zbekiston Respublikasi Axborot texnologiyalari va kommunikatsiyalarini rivojlantirish vazirligi<br>
            © 2006-2017<br>
            <p><a href="/uz/pages/order">Ma’lumotdan foydalanish shartlari</a>
            </p>

            <div class="social">
                <span>Ijtimoiy tarmoqlarda:</span>
                <ul>
                    <li><a href="https://www.facebook.com/mininfocom" target="_blank"><img src="/img/facebook.png" alt=""></a></li>
                    <li><a href="https://www.youtube.com/channel/UC5xqeoxaAstItOP7UOFXRAA" target="_blank"><img src="/img/youtube.png" alt=""></a></li>
                    <li><a href="https://t.me/mitcuz" target="_blank"><img src="/img/telegram.png" alt=""></a></li>
                    <!--            <li><a href="#" target="_blank"><img src="--><!--/img/davra.png" alt=""></a></li>-->
                </ul>
            </div>
        </div>
        <div class="col-sm-4 nopadeleft foot_text">
            <p>100047, Toshkent Amir Temur shoh ko’chasi 4</p>
            <p>Tel: (998 71) 238-41-07, faks (998 71) 239-87-82</p>
            <p>E-mail: info@mitc.uz</p>
            <p>Sayt ishlab chiqilgan:        <a href="http://uzinfocom.uz/"><img src="/img/uzinfocom.png"></a></p>
        </div>
        <div class="col-sm-4 nopadeleft foot_text text_left">
            <ul class="way_list">
                <li><a href="http://lex.uz/pages/getpage.aspx?lact_id=3107036#3109519" target="_blank"><img src="/media/8d8d9b7c-13a2-bff1-6765-cfb1e6c28dc9.jpg"></a></li>
                <li>
                    <!-- START WWW.UZ TOP-RATING --><script language="javascript" type="text/javascript">
                        <!--
                        top_js="1.0";top_r="id=28926&r="+escape(document.referrer)+"&pg="+escape(window.location.href);document.cookie="smart_top=1; path=/"; top_r+="&c="+(document.cookie?"Y":"N")
                        //-->
                    </script>
                    <script language="javascript1.1" type="text/javascript">
                        <!--
                        top_js="1.1";top_r+="&j="+(navigator.javaEnabled()?"Y":"N")
                        //-->
                    </script>
                    <script language="javascript1.2" type="text/javascript">
                        <!--
                        top_js="1.2";top_r+="&wh="+screen.width+'x'+screen.height+"&px="+
                            (((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth)
                        //-->
                    </script>
                    <script language="javascript1.3" type="text/javascript">
                        <!--
                        top_js="1.3";
                        //-->
                    </script>
                    <script language="JavaScript" type="text/javascript">
                        <!--
                        top_rat="&col=26477A&t=FFFFFF&p=FFFFFF";top_r+="&js="+top_js+"";document.write('<a href="http://www.uz/rus/toprating/cmd/stat/id/28926" target=_top><img src="http://www.uz/plugins/top_rating/count/cnt.png?'+top_r+top_rat+'" width=88 height=31 border=0 alt="Топ рейтинг www.uz"></a>')//-->
                    </script>
                    <!--a href="http://www.uz/rus/toprating/cmd/stat/id/28926" target="_top"><img src="http://www.uz/plugins/top_rating/count/cnt.png?id=28926&amp;r=http%3A//ccitt.uz/ru/&amp;pg=http%3A//ccitt.uz/ru/%3Fbitrix_include_areas%3DY&amp;c=Y&amp;j=Y&amp;wh=1280x1024&amp;px=32&amp;js=1.3&amp;col=26477A&amp;t=FFFFFF&amp;p=FFFFFF" width="88" height="31" border="0" alt="Топ рейтинг www.uz"></a-->
                    <noscript>&amp;amp;amp;amp;lt;A href="http://www.uz/rus/toprating/cmd/stat/id/28926" target=_top&amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;IMG height=31 src="http://www.uz/plugins/top_rating/count/nojs_cnt.png?id=28926&amp;amp;amp;amp;amp;pg=http%3A//ccitt.uz&amp;amp;amp;amp;amp;&amp;amp;amp;amp;amp;col=26477A&amp;amp;amp;amp;amp;amp;t=FFFFFF&amp;amp;amp;amp;amp;amp;p=FFFFFF" width=88 border=0 alt="Топ рейтинг www.uz"&amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;/A&amp;amp;amp;amp;gt;</noscript>
                    <!-- FINISH WWW.UZ TOP-RATING -->
                </li>
                <li><a href="http://uforum.uz/" target="_blank"><img src="/img/u_forum.png"></a></li>
                <li><a href="https://my.gov.uz/" target="_blank"><img src="/img/my_gov.png"></a></li>
                <li><a href="https://www.id.uz/" target="_blank"><img src="/img/id_uz.png"></a></li>
                <li><a href="http://www.cert.uz/" target="_blank"><img src="/img/uz_cert.png"></a></li>
                <li><a href="http://dc.uz/" target="_blank"><img src="/img/data_center.png"></a></li>
            </ul>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade menu_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class=" head_menu_modal">
                    <nav id="w17" class="menu navbar" role="navigation"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w17-collapse"><span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span></button></div><div id="w17-collapse" class="collapse navbar-collapse"><ul id="w18" class="navbar-nav nav"><li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Vazirlik haqida <b class="caret"></b></a><ul id="w19" class="dropdown-menu"><li class=""><a href="/uz/pages/about_ministry" target="" tabindex="-1">Vazirlik haqida</a></li>
                                            <li class=""><a href="/uz/pages/board" target="" tabindex="-1">Hay`at</a></li>
                                            <li class=""><a href="/uz/management/index" target="" tabindex="-1">Rahbariyat</a></li>
                                            <li class=""><a href="/uz/pages/ministry_structure" target="" tabindex="-1">Vazirlik tuzilmasi</a></li>
                                            <li class=""><a href="/uz/management/head" target="" tabindex="-1">Markaziy apparat</a></li>
                                            <li class=""><a href="/uz/management/subordinate" target="" tabindex="-1">Hududiy boshqarmalar</a></li>
                                            <li class=""><a href="/uz/pages/Offers_ministry" target="" tabindex="-1">Vazirlikka murojaat</a></li>
                                            <li class=""><a href="/uz/management/system" target="" tabindex="-1">Tizimdagi tashkilotlar</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Axborot xizmati <b class="caret"></b></a><ul id="w20" class="dropdown-menu"><li class=""><a href="/uz/news/category/1" target="" tabindex="-1">Vazirlik yangiliklari</a></li>
                                            <li class=""><a href="/uz/news/category/2" target="" tabindex="-1">OAV AKT haqida</a></li>
                                            <li class=""><a href="/uz/news/category/4" target="" tabindex="-1">Аnons</a></li>
                                            <li class=""><a href="/uz/news/category/5" target="" tabindex="-1">E&#039;lonlar</a></li>
                                            <li class=""><a href="/uz/pages/speeches_statements" target="" tabindex="-1">Rahbariyatning chiqish va bayonotlari</a></li>
                                            <li class=""><a href="/uz/pages/publications_communications_informatization" target="" tabindex="-1">Soha nashrlari</a></li>
                                            <li class=""><a href="/uz/pages/publications" target="" tabindex="-1">Nashrlar</a></li>
                                            <li class=""><a href="/uz/news/vfiles" target="" tabindex="-1">Videolavhalar</a></li>
                                            <li class=""><a href="/uz/pages/major_events" target="" tabindex="-1">Мatbuot anjumanlari jadvali</a></li>
                                            <li class=""><a href="/uz/pages/media_plan" target="" tabindex="-1">Media-reja</a></li>
                                            <li class=""><a href="/uz/news/accreditation" target="" tabindex="-1">OAV vakillarini akkreditasiyadan o&#039;tkazish</a></li>
                                            <li class=""><a href="/uz/pages/press_contact" target="" tabindex="-1">Axborot xizmati bilan bog&#039;lanish</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Faoliyat <b class="caret"></b></a><ul id="w21" class="dropdown-menu"><li class=""><a href="/uz/pages/communication" target="" tabindex="-1">Telekommunikatsiya</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                            <li class=""><a href="/uz/pages/actions_strategy" target="" tabindex="-1">Harakatlar strategiyasi</a></li>
                                            <li class=""><a href="/uz/pages/information_technologies" target="" tabindex="-1">Axborot texnologiyalari</a></li>
                                            <li class=""><a href="/uz/pages/post" target="" tabindex="-1">Pochta</a></li>
                                            <li class=""><a href="/uz/pages/ict_education" target="" tabindex="-1">AKTda ta&#039;lim</a></li>
                                            <li class=""><a href="/uz/pages/international_relations" target="" tabindex="-1">Xalqaro aloqalar</a></li>
                                            <li class=""><a href="/uz/pages/council_orders" target="" tabindex="-1">Jamoatchilik maslahat kengashi</a></li>
                                            <li class=""><a href="/uz/pages/e_reestr" target="" tabindex="-1">Davlat xizmatlari reestri</a></li>
                                            <li class=""><a href="/uz/pages/about_fond" target="" tabindex="-1">AKT ni rivojlantrish jamg`armasi</a></li>
                                            <li class=""><a href="/uz/pages/reports_of_ministry" target="" tabindex="-1">Hisobotlar</a></li>
                                            <li class=""><a href="/uz/pages/anti-corruption" target="" tabindex="-1">Korrupsiyaga qarshi kurash</a></li>
                                            <li class=""><a href="/uz/pages/inves_tory" target="" tabindex="-1">AKT ga investitsiyalar</a></li>
                                            <li class=""><a href="/uz/pages/list_of_services" target="" tabindex="-1">Interaktiv davlat xizmatlari ro&#039;yxati</a></li>
                                            <li class=""><a href="/uz/pages/cooperation_with_other_org" target="" tabindex="-1">Boshqa tashkilotlar bilan hamkorlik</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Hujjatlar <b class="caret"></b></a><ul id="w22" class="dropdown-menu"><li class=""><a href="/uz/pages/Documents/615" target="" tabindex="-1">Qonunlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/623" target="" tabindex="-1">Prezident qarorlari va farmonlari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/622" target="" tabindex="-1">Hukumat qarori va farmoyishlari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/1208" target="" tabindex="-1">Tadbirkorlikka oid hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/621" target="" tabindex="-1">Qo‘shma qarorlar</a></li>
                                            <li class=""><a href="/uz/pages/Documents/620" target="" tabindex="-1">Davlat standarti</a></li>
                                            <li class=""><a href="/uz/pages/Documents/618" target="" tabindex="-1">Adliya vazirligi tomonidan ro‘yxatga olingan me’yoriy hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/regulation" target="" tabindex="-1">Me’yoriy hujjatlar loyihalari</a></li>
                                            <li class=""><a href="/uz/pages/Documents/616" target="" tabindex="-1">Kuchini yo‘qotgan me’yoriy hujjatlar</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/#" target="" data-toggle="dropdown">Boshqaruv <b class="caret"></b></a><ul id="w23" class="dropdown-menu"><li class=""><a href="/uz/pages/regulation/489" target="" tabindex="-1">Davlat nazorati</a></li>
                                            <li class=""><a href="/uz/pages/regulation/627" target="" tabindex="-1">Litsenziyalash</a></li>
                                            <li class=""><a href="/uz/pages/regulation/626" target="" tabindex="-1">Sertifikatlashtirish</a></li>
                                            <li class=""><a href="/uz/pages/regulation/625" target="" tabindex="-1">Standartlashtirish</a></li>
                                            <li class=""><a href="/uz/pages/communication/553" target="" tabindex="-1">Radiochastota spektridan foydalanishni boshqarish</a></li></ul></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="/uz/pages/egovernment/3038" target="" data-toggle="dropdown">Elektron hukumat <b class="caret"></b></a><ul id="w24" class="dropdown-menu"><li class=""><a href="/uz/pages/egovernment/3034" target="" tabindex="-1">Elektron hukumatga oid me&#039;yoriy-huquqiy hujjatlar</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3037" target="" tabindex="-1">&quot;Elektron hukumat&quot; tizimi</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3035" target="" tabindex="-1">Elektron hukumat bo&#039;yicha loyihalar</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/2074" target="" tabindex="-1">O&#039;zbekistonda elektron hukumatni joriy etish va rivojlantirish holati</a></li>
                                            <li class=""><a href="/uz/pages/egovernment/3038" target="" tabindex="-1">&quot;Elektron Hukumat&quot; tuzilmasi</a></li></ul></li>
                                    <li class=""><a href="/uz/pages/contacts" target="">Bog&#039;lanish</a></li></ul></div></div></nav>                </div>
            </div>
        </div>
    </div>
</div>



<script src="/js/jquery-2.1.3.min.js"> </script>

<script src="/assets/c8a834d/jquery.js"></script>
<script src="/assets/1c73605d/yii.js"></script>
<script src="/js/jquery-ui.js"></script>
<script src="/js/jquery.cookie.js"></script>
<script src="/js/jQuery.print.js"></script>
<script src="/js/mousetrap.min.js"></script>
<script src="/js/specialView.js"></script>
<script src="/js/jquery.vmap.js"></script>
<script src="/js/waypoints.min.js"></script>
<script src="/js/map.js"></script>
<script src="/js/orphus.js"></script>
<script src="/js/pie.js"></script>
<script src="/js/scroll.js"></script>
<script src="/js/Chart.js"></script>
<script src="/js/highcharts.js"></script>
<script src="/js/exporting.js"></script>
<script src="/js/highcharts-more.js"></script>
<script src="/js/zoom.js"></script>
<script src="/js/main.js"></script>
<script src="/js/hoverIntent.js"></script>
<script src="/assets/3c58d997/js/bootstrap.js"></script>
<script type="text/javascript">jQuery(document).ready(function () {
        var messages = {
            an: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img  class="img_modal_dep" src="/media/ccb352be-25e8-81b4-3204-1424644287da.jpg">' +
            '<div class="mini_modal_title">Andijon viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Usmanov Lazizbek Xaydaralievich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 374 226-03-55, Факс: 226-22-90;</p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depandijan@mitc.uz</p>' +
            '</div>' +
            '</div>',
            bu: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep" src="/media/86cb202e-cc99-d443-7e33-09516b91ce71.jpg">' +
            '<div class="mini_modal_title">Buxoro viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Allayev Faxriddin Jamolovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 365 223-04-40, Faks: 223-14-84</p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depbukhara@mitc.uz</p>' +
            '</div>' +
            '</div>',
            fa: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/c97b4140-a4a8-3f7a-a955-a6ddf31c4561.jpg">' +
            '<div class="mini_modal_title">Farg`ona viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Karimov Javlonbek Mirzag`afforovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 373 244-44-44, Faks: 244 44 45; </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depfergana@mitc.uz</p>' +
            '</div>' +
            '</div>',
            gu: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/82fa7987-6013-751f-4709-e31cf95ed978.jpg">' +
            '<div class="mini_modal_title">Sirdaryo viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Xidirov Baxodir Irgashevich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 367 235 00 10, Faks: 225-32-99; </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depgulistan@mitc.uz</p>' +
            '</div>' +
            '</div>',
            ji: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/1e69d1c9-432d-e545-bc44-9c53dcdeeaab.jpg">' +
            '<div class="mini_modal_title">Jizzax viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Savurbayev Akmal Abdumuminovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i v.v.b</p>' +
            '<p class="f16">Telefon: +998 (0) 372 226-21-84, Faks: 226-62-81  </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depdjizak@mitc.uz</p>' +
            '</div>' +
            '</div>',
            na: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/cfb7c2f5-fbdb-c25c-2567-40c443306aa0.jpg">' +
            '<div class="mini_modal_title">Navoiy viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Teshaev Fozil Ismatulloevich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 436 223-82-82, Факс: 223-04-34;  </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depnavoi@mitc.uz</p>' +
            '</div>' +
            '</div>',
            no: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/1f5a8cc3-9e21-ff39-b7fe-d671d15141cd.jpg">' +
            '<div class="mini_modal_title">Namangan viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Ishanov Yusupxan Umarxanovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig‘i</p>' +
            '<p class="f16">Telefon: +998 (0) 369 227-12-61, Faks: 227-24-36;    </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depnamangan@mitc.uz</p>' +
            '</div>' +
            '</div>',
            qa: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/7ba2b84c-19a4-5155-d733-0e74c1c5a7f8.jpg">' +
            '<div class="mini_modal_title">Qashqadaryo viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Aripov Sandjar Kamalovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 375 221-12-08, Faks: 221-80-00; </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depkarshi@mitc.uz</p>' +
            '</div>' +
            '</div>',
            qo: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/fedc99d7-500f-86c5-884e-52ac18935c1d.jpg">' +
            '<div class="mini_modal_title">Qoraqalpog’iston Respublikasi hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Mnajov Berdax Niyetbayevich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 361 222-14-41 Faks: 222-14-51;  </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depnukus@mitc.uz</p>' +
            '</div>' +
            '</div>',
            sa: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/2d1fd66d-9d04-e09b-25a5-17b40b7b2aa6.jpg">' +
            '<div class="mini_modal_title">Samarqand viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Eshnazarov Nodirber Tirkashevich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 366 234-42-10, Факс: 234-12-90;   </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depsamarkand@mitc.uz</p>' +
            '</div>' +
            '</div>',
            te: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/1cbc2d89-53ff-9720-42c7-b3e5d512207f.JPG">' +
            '<div class="mini_modal_title">Surxandaryo viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Saidov Fayzulla Xolmuhammadovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 376 223-46-84 Faks: 223-46-39; </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">deptermez@mitc.uz</p>' +
            '</div>' +
            '</div>',
            tosh: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/227ff922-8389-fc1a-4585-f31d44ce7db3.jpg">' +
            '<div class="mini_modal_title">Toshkent shahar hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Arifdjanov Akmal Zakirdjanovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig‘i</p>' +
            '<p class="f16">Telefon: +998 (0) 371 244-30-03, Faks: 244-47-07 </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">deppoytaxt@mitc.uz</p>' +
            '</div>' +
            '</div>',
            to: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/7b3bb402-ebca-6e1a-8b8b-c79ed2471dbb.jpg">' +
            '<div class="mini_modal_title">Toshkent viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Matkarimov Abdufarid Abdurashidovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 371 281-60-30, Faks: 281-47-91;  </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">deptashkent@mitc.uz</p>' +
            '</div>' +

            '</div>',
            xo: '<div class="mini_modal">' +
            '<i></i>' +
            '<b class="x_out"></b>' +
            '<img class="img_modal_dep"  src="/media/5f332d4f-9cfa-1de2-64e7-dc2244b70acc.jpg">' +
            '<div class="mini_modal_title">Xorazm viloyati hududiy boshqarmasi</div>' +
            '<div class="mini_modal_text">' +
            '<p class="f18 ptbold">Xayitbaev Qoshnazar Sobirovich</p>' +
            '<p class="f16">Lavozimi: Boshqarma boshlig’i </p>' +
            '<p class="f16">Telefon: +998 (0) 362 224-52-52, Faks: 225-57-17 </p>' +
            '<p class="f16 ptbold">Qabul kunlari: dushanba-juma 09:00 - 12:00</p>' +
            '<p class="f16">depurgench@mitc.uz</p>' +
            '</div>' +
            '</div>'
        };
        makeMapEnd(messages);




        $(document).ready(function () {
            doNut(
                className = 'd1',
                colors = ['#629c6d','#8aca96'],
                arrayData = [{
                    y: 45.33,
                },]
            );

            doNut(
                className = 'd2',
                colors = ['#627f9c','#8ab0d6'],
                arrayData = [{
                    y: 25.33,
                },]
            );

            doNut(
                className = 'd3',
                colors = ['#c67e7e'],
                arrayData = [{
                    y: 99.33,
                }]
            );

            doNut(
                className = 'd4',
                colors = ['#8b7b9f','#dad0e6'],
                arrayData = [{
                    y: 65.33,
                }]
            );

            doNut(
                className = 'd5',
                colors = ['#b6e8de','#7b9f98'],
                arrayData = [{
                    y: 6.33,
                },{
                    y: 70.03,
                }]
            );
        });

    });</script>

</body>
</html>
